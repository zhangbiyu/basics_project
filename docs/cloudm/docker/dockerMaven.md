# Docker
> docker 常用功能笔记

![](images/Docker.png)


- [安装](/cloudm/docker/dockerMaven?id=安装)
- [docker-安装-redis](/cloudm/docker/dockerMaven?id=docker-安装-redis)
- [docker-安装-rabbitmq](/cloudm/docker/dockerMaven?id=docker-安装-rabbitmq)
- [docker-使用-compose](/cloudm/docker/dockerMaven?id=docker-使用-compose)
- [docker-构建镜像（maven）](/cloudm/docker/dockerMaven?id=docker-构建镜像（maven）)
- [docker-构建镜像并上传的远程仓库](/cloudm/docker/dockerMaven?id=docker-构建镜像并上传的远程仓库)
- [docker-常用命令](/cloudm/docker/dockerMaven?id=docker-常用命令)
- [docker-多主机互联](/cloudm/docker/dockerMaven?id=docker-多主机互联)

## 安装
> centos 版本安装

``` bash
# 移除旧的版本：
  $ sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-selinux \
                  docker-engine-selinux \
                  docker-engine

# 安装一些必要的系统工具：
  sudo yum install -y yum-utils device-mapper-persistent-data lvm2

# 添加软件源信息：
  sudo yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

# 更新 yum 缓存：
  sudo yum makecache fast

# 安装 Docker-ce：
  sudo yum -y install docker-ce

# 启动 Docker 后台服务
  sudo systemctl start docker
```

## Docker 安装 redis
> redis docker 仓库地址： [https://hub.docker.com/_/redis](https://hub.docker.com/_/redis)

##### 1. 选择最新版latest

``` bash
docker pull redis:latest
```

##### 2. 创建容器并设置密码

``` bash
docker run --name redis -p 6379:6379 -d --restart=always redis:latest redis-server --appendonly yes --requirepass 'Hao123baidu'
```

?> `-p` 6379:6379 :将容器内端口映射到宿主机端口(右边映射到左边)<br>
`redis-server –appendonly yes` : 在容器执行redis-server启动命令，并打开redis持久化配置<br>
`requirepass “your passwd”` :设置认证密码<br>
`–restart=always` : 随docker启动而启动<br>

## Docker 安装 RabbitMQ
> RabbitMQ docker 仓库地址：[https://hub.docker.com/_/rabbitmq](https://hub.docker.com/_/rabbitmq)

##### 1. 拉取镜像

``` bash
docker search rabbitmq:management
docker pull rabbitmq:management
```

##### 2. 启动镜像（默认用户名密码）,默认guest 用户，密码也是 guest

``` bash
docker run -d --hostname my-rabbit --name rabbit -p 15672:15672 -p 5672:5672 rabbitmq:3-management
```

##### 3. 启动镜像（设置用户名密码）

```bash
docker run -d --hostname my-rabbit --name rabbit -e RABBITMQ_DEFAULT_USER=user -e RABBITMQ_DEFAULT_PASS=password -p 15672:15672 -p 5672:5672 rabbitmq:3-management
```

##### 4. 完成后访问：http://localhost:15672/

## docker 使用 compose

> 安装compose

1. 运行以下命令以下载 Docker Compose 的当前稳定版本：

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```
2. 将可执行权限应用于二进制文件：

```bash
sudo chmod +x /usr/local/bin/docker-compose
```

3. 创建软链：

```bash
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

4. 测试是否安装成功：

``` bash
docker-compose --version

docker-compose version 1.25.0, build 1110ad01
```
> 编写compose文件

``` yaml
version: '3'
services:
  nginx:
    image: nginx
    container_name: nginx
    volumes:
      - ./nginx/html:/usr/share/nginx/html
      - ./nginx/nginx.conf:/etc/nginx/nginx.conf
      - ./nginx/logs:/var/log/nginx
    ports:
      - "80:80"
      - "443:443"
    command: [nginx, '-g', 'daemon off;']

  # api服务
  ant_api:
    image: ascdc/jdk8
    ports:
      - 8010:8010
    restart: 'always'
    container_name: ant_api
    environment:
      TZ: Asia/Shanghai
    volumes:
      - ./ant_api:/data/
    entrypoint: java -jar /data/webapps/ant_api.jar

```

!> nginx 安装可能会报错，如果报错的话，先注释掉 `- ./nginx/nginx.conf:/etc/nginx/nginx.conf`。然后cp 镜像内的`nginx.conf`到服务器的本地目录，然后再次运行docker-compose

> docker-compose 命令

``` bash
#查看帮助
docker-compose -h

# -f  指定使用的 Compose 模板文件
# 默认为 docker-compose.yml，可以多次指定。
docker-compose -f docker-compose.yml up -d 

#启动所有容器，-d 将会在后台启动并运行所有的容器
docker-compose up -d

#停用移除所有容器以及网络相关
docker-compose down

#查看服务容器的输出
docker-compose logs

#列出项目中目前的所有容器
docker-compose ps

#构建（重新构建）项目中的服务容器。服务容器一旦构建后，将会带上一个标记名.
#例如对于 web 项目中的一个 db 容器，可能是 web_db。
#可以随时在项目目录下运行 docker-compose build 来重新构建服务
docker-compose build

# 不带缓存的构建。
docker-compose build --no-cache

#拉取服务依赖的镜像
docker-compose pull

#重启项目中的服务
docker-compose restart

#删除所有（停止状态的）服务容器。
#推荐先执行 docker-compose stop 命令来停止容器。
docker-compose rm 

#在指定服务上执行一个命令。
docker-compose run ubuntu ping docker.com

#设置指定服务运行的容器个数。通过 service=num 的参数来设置数量
docker-compose scale web=3 db=2

#启动已经存在的服务容器。
docker-compose start

#停止已经处于运行状态的容器，但不删除它。
#通过 docker-compose start 可以再次启动这些容器。
docker-compose stop

```

## Docker 构建镜像（Maven）
> 此处介绍 使用Maven来构建Docker镜像并上传到私服仓库

##### 1. 开启docker远程API端口

``` bash
# 修改docker配置文件
  vi /usr/lib/systemd/system/docker.service

# 在ExecStart后面添加 -H unix:///var/run/docker.sock -H 0.0.0.0:2375

# 重新加载配置文件
  systemctl daemon-reload

# 重启docker
  systemctl restart docker
```

##### 2. 添加maven docker插件

``` xml
  <!-- docker 配置-->
  <plugin>
      <groupId>com.spotify</groupId>
      <artifactId>docker-maven-plugin</artifactId>
      <version>1.2.0</version>
      <configuration>
          <forceTags>true</forceTags>
          <imageName>grasp-spring</imageName>
          <baseImage>azul/zulu-openjdk:8</baseImage>
          <entryPoint>["java", "-jar", "/${project.build.finalName}.jar"]</entryPoint>
          <dockerHost>http://192.168.1.45:2375</dockerHost>
          <resources>
              <resource>
                  <targetPath>/</targetPath>
                  <directory>${project.build.directory}</directory>
                  <include>${project.build.finalName}.jar</include>
              </resource>
          </resources>
      </configuration>
  </plugin>
```

##### 3. 执行maven 打包命令

``` bash
  mvn clean package docker:build
```

## Docker 构建镜像并上传的远程仓库

##### 1. 创建docker远程仓库

> 去[docker hub](https://hub.docker.com)官网注册账号，并创建远程仓库

##### 2. 修改本地maven配置文件 settings.xml
添加docker hub账号和密码

``` xml
<servers>
  <server>
    <id>docker-hub</id>
    <username>你的DockerHub用户名</username>
    <password>你的DockerHub密码</password>
    <configuration>
      <email>你的DockerHub邮箱</email>
    </configuration>
  </server>
</servers>
```

##### 3. 项目pom.xml修改为如下：注意imageName的路径要和repo的路径一致

``` xml
  <plugin>
      <groupId>com.spotify</groupId>
      <artifactId>docker-maven-plugin</artifactId>
      <version>1.2.0</version>
      <configuration>
          <forceTags>true</forceTags>
          <imageName>zhangbiyu/grasp-spring</imageName>
          <baseImage>azul/zulu-openjdk:8</baseImage>
          <entryPoint>["java", "-jar", "/${project.build.finalName}.jar"]</entryPoint>
          <dockerHost>http://192.168.1.45:2375</dockerHost>
          <resources>
              <resource>
                  <targetPath>/</targetPath>
                  <directory>${project.build.directory}</directory>
                  <include>${project.build.finalName}.jar</include>
              </resource>
          </resources>
          <serverId>docker-hub</serverId>
          <registryUrl>zhangbiyu/grasp-spring</registryUrl>
      </configuration>
  </plugin>
```

##### 4. 执行打包命令

``` bash
  mvn clean package docker:build  docker:push
```

## Docker 常用命令
> 日常用到的命令，docker基础命令跳转 [runoob](http://www.runoob.com/docker/docker-command-manual.html)

``` bash
# 1. 进入docker容器内部
  docker exec -it xxx /bin/bash 

# 2.同步docker容器与宿主机时间
  docker cp /etc/localtime 8ea91b2f6274:/etc/

# 3.删除镜像为none的镜像
  docker rmi $(docker images | grep "none" | awk '{print $3}')

# 4.查看指定时间后的日志，只显示最后100行
  docker logs -f -t --since="2018-02-08" --tail=100 CONTAINER_ID


```
## docker-多主机互联

> 多主机的网络直连需求源于ECS VM容器化服务治理业务场景。 在此场景下，Docker容器运行在阿里云的ECS VM之中， Docker传统的网络解决方案
采用NAT的方式实现容器间互联，不同宿主机的容器之间无法感知到真实ip地址，因此无法满足如配置中心这样的架构。而ECS又不支持给VM独立增加IP，
从而迫使我们在短期内只能一个VM跑一个Docker的方案，势必会造成资源的浪费。但是docker内部的Swarm方式中的Overlay的底层是Vxlan通信方式，
而ECS VM底层的通信也是Vxlan。所以任何基于Overlay实现的方案都不可选而我们选择的方案是Pipework,他切割了Docker容器的网络平面，
提供了VM运行多Docker容器的能力，能大大提高资源的利用率，降低成本。
!> 此方案仅限制于私有云环境，如果是公有云建议专用的容器云。

> pipework原理：基于LAN的网络环境自建一个网桥用来桥接主机中的eth0网桥,然后我们通过自己指定Ip就可以实现容器互联了。

![](https://tva1.sinaimg.cn/large/0082zybply1gc7a2lowlfj310l0u04je.jpg)

此操作的前提条件是机器上已经安装了docker服务，并且服务开启，有镜像可以使用。

>第一步：安装网桥设备,安装完成之后就可以通过brctl命令来查看网桥配置

```shell script
yum install -y bridge-utils
```

>第二步：删除当前eth0(有的计算机可能不是这个文件名)网桥的配置(注释掉IP、子网掩码、网关、DNS、)

```shell script
vim /etc/sysconfig/network-scripts/ifcfg-eth0
```

```shell script
TYPE=Ethernet
BOOTPROTO=none
DEFROUTE=yes
PEERROUTES=yes
PEERDNS=yes
NAME=eth0
UUID=00f2e830-ed07-4ace-9e54-56a325e3a690
ONBOOT=yes
#IPADDR0=192.168.20.150
#PREFIX0=24
#GATEWAY0=192.168.20.254
#DNS1=192.168.61.20.254
HWADDR=00:0C:29:F7:22:81
BRIDGE="br-0" # 关联到新的网卡:br-0中
```

> 第三步：设置新的网桥br-0

```shell script
vim /etc/sysconfig/network-scripts/ifcfg-br-0
```

```shell script
TYPE=Bridge
BOOTPROTO=static
IPADDR=192.168.20.150
NETMASK=255.255.255.0
GATEWAY=192.168.20.254
PREFIX=24
DNS1=192.168.20.254
NAME=br-0
ONBOOT=yes
DEVICE=br-0
```

> 第四步：重启网卡会看到我们新建的网卡
```shell script
systemctl restart network
```

> 第五步：新建容器

注意这里的网络模式是:--net=none

```shell script
[root@docker ~]# docker images
REPOSITORY            TAG                IMAGE ID               CREATED                        SIZE
docker.io/zookeeper   latest              19604ac4a163        Less than a second ago   143 MB
redis                           latest               07818b5b6de8        8 hours ago                      482.9 MB
[root@docker ~]# docker run -it -d --net=none --name ip-test redis /bin/bash
[root@docker ~]# docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
4190f301a367        redis               "/bin/bash"         55 minutes ago      Up 55 minutes                           ip-test
```

>第六步：获取pipework设置Ip

```shell script
[root@docker ~]# git clone https://github.com/jpetazzo/pipework.git
[root@docker ~]# cd pipework/
[root@docker pipework]# ls
docker-compose.yml  doctoc  LICENSE  pipework  pipework.spec  README.md
[root@docker ~]# cd ..
[root@docker ~]# cp -rp pipework/pipework /usr/local/bin
[root@docker ~]# pipework br-ex ip-test 192.168.20.100/24@192.168.20.254
```

>第七步：验证IP设置是否正确
```shell script
[root@docker ~]# ping 192.168.20.100
PING 192.168.61.100 (192.168.61.100) 56(84) bytes of data.
64 bytes from 192.168.61.100: icmp_seq=1 ttl=64 time=0.206 ms
64 bytes from 192.168.61.100: icmp_seq=2 ttl=64 time=0.081 ms
^C
--- 192.168.61.100 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 0.081/0.143/0.206/0.063 ms
```
