# Mariadb
> mariadb 常用功能笔记

![](images/mariadb.png)

- [安装](/database/mariadb/mariadbDesc?id=安装)
- [主从同步](/database/mariadb/mariadbDesc?id=主从同步)
- [数据库中间件](/database/mariadb/mariadbDesc?id=数据库中间件)

## 安装
> Installation4Docker 仓库地址： [https://hub.docker.com/_/mysql/](https://hub.docker.com/_/mysql/)

``` bash
docker run --name mariaServer -p 1106:3306 -v /usr/local/software/mariadb/:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 -d mariadb
```
?> `–name`：给新创建的容器命名，此处命名为mysqlserver <br>
`-e`：配置信息，此处配置mysql的root用户的登陆密码 <br>
`-p`：端口映射，表示在这个容器中使用3306端口(第二个)映射到本机的端口号也为3306(第一个) <br>
`-d`：成功启动容器后输出容器的完整ID <br>

## 主从同步
> Master to sync4 Docker images
```shell script
# 创建数据卷
mkdir -p /usr/local/software/mariadb/
mkdir -p /usr/local/software/mariadb1/
```

```shell script
# 运行主容器
docker run --name master -p 1106:3306 -v /usr/local/software/mariadb/:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 -d mariadb
```
```shell script
# 从容器中拷贝一份mariadb配置文件
sudo docker cp master:/etc/mysql/my.cnf /usr/local/software/mariadb/master_my.cnf
```

```shell script
# 修改master_my.cnf，在 [mysqld] 节点下添加
server-id=1
log_bin=master-bin
binlog-ignore-db=mysql
binlog-ignore-db=information_schema
binlog-ignore-db=performance_schema
binlog-ignore-db=test
innodb_flush_log_at_trx_commit=1
binlog_format=mixed
```

```shell script
# 复制master_my.cnf并覆盖master中的my.cnf
docker cp /usr/local/software/mariadb/master_my.cnf master:/etc/mysql/my.cnf
```

```shell script
# 重启master容器
docker restart master
```
```shell script
# 进入主容器
docker exec -it master /bin/bash
mysql -uroot -p123456
MariaDB [(none)]> SHOW MASTER status;
# 得到master的log文件及其位置
如File 为 master-bin.000002,Position为632
```
```shell script
# 运行从容器
docker run --name slave -p 1107:3306 -v /usr/local/software/mariadb1/:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 -d mariadb
```
```shell script
# 拷贝一份MySQL配置文件
sudo docker cp slave:/etc/mysql/my.cnf /usr/local/software/mariadb1/slave_my.cnf
# 修改slave1_my.cnf，在 [mysqld] 节点下添加
server-id=2
relay-log-index=slave-relay-bin.index
relay-log=slave-relay-bin
relay_log_recovery=1
# 复制slave1_my.cnf并覆盖slave1中的my.cnf
docker cp /usr/local/software/mariadb1/slave_my.cnf slave:/etc/mysql/my.cnf
```
```shell script
# 重启从容器
docker restart slave
# 进入容器并执行命令
docker exec -it slave /bin/bash
mysql -uroot -p123456
MariaDB [(none)]> stop SLAVE;
MariaDB [(none)]> change master to
 MASTER_HOST='10.18.139.141',
 MASTER_USER='root',
 MASTER_PASSWORD='123456',
 MASTER_PORT=23306,
 MASTER_LOG_FILE='master-bin.000002',
 MASTER_LOG_POS=632;
MariaDB [(none)]> start SLAVE;
MariaDB [(none)]> show slave status;
# 查看运行结果，当结果显示为如下则代表搭建成功
Slave_IO_State:Waiting for master to send event
Slave_IO_Running:Yes
Slave_SQL_Running:Yes
# 解释
备注:
MASTER_HOST 连接主容器ip地址(宿主机的ip地址)
MASTER_PORT 主容器的端口
MASTER_USER 同步账号的用户名
MASTER_PASSWORD 同步账号的密码
MASTER_LOG_FILE 主容器的日志地址(之前查到)
MASTER_LOG_POS 主容器的日志位置(之前查到)
```
- 添加更多从容器和以上方法一样

```shell script
# 进行测试,在主容器中执行
create database dakraw;
# 如果从容器中也能看到此数据库代表完成主从的配置
```

> 设置从容器只读
```shell script
# 设置只读为0,读写为1
set global super_read_only=0;
# 查看是否设置成功s
show global variables like "%read_only%";
```

> 可能会遇到的问题
- Mysql主从库不同步1236错误：could not find first log file name in binary log index file 错误是主从的一个日志问题
根本原因是因为在主容器中执行 `SHOW MASTER status;` 的时候会显示一个表格,将对应的File和Position修改到从容器中执行命令 `change master to MASTER_HOST='10.18.139.141',MASTER_USER='root',MASTER_PASSWORD='123456',MASTER_PORT=23306,MASTER_LOG_FILE='master-bin.000002',MASTER_LOG_POS=632;`
中的`MASTER_LOG_FILE` 和 `MASTER_LOG_POS` 中即可。

## 数据库中间件

> [DBLE](https://actiontech.github.io/dble-docs-cn/)-[官方视频](https://opensource.actionsky.com/category/documents/about-dble/):是上海爱可生信息技术股份有限公司基于mysql的高可扩展性的分布式中间件
> 优势特性：
- `数据水平拆分` 随着业务的发展，您可以使用dble来替换原始的单个MySQL实例。
- `兼容Mysql` 与MySQL协议兼容，在大多数情况下，您可以用它替换MySQL来为你的应用程序提供新的存储，而无需更改任何代码。
- `高可用性` dble服务器可以用作集群，业务不会受到单节点故障的影响。
- `SQL支持` 支持SQL 92标准和MySQL方言。我们支持复杂的SQL查询，如group by，order by，distinct，join，union，sub-query等等。
- `复杂查询优化` 优化复杂查询，包括但不限于全局表连接分片表，ER关系表，子查询，简化选择项等。
- `分布式事务支持` 使用两阶段提交的分布式事务。您可以为了性能选择普通模式或者为了数据安全采用XA模式。当然，XA模式依赖于MySQL-5.7的XA Transaction，MySQL节点的高可用性和数据的可靠性。

> 缺点：
- `Mysql/Mariadb` 只支持Mysql/Mariadb
- `myBatis/Hibernate` dble虽然支持Hibernate但不建议使用Hibernate，因为Hibernate无法控制SQL的生成，无法做到对查询SQL的优化，大数量下可能会出现性能问题。
- `druid` INSERT ... VALUE ... 和 INSERT ... VALUES ...这个两个语句中，VALUE[S]关键字必须写正确，否则会出现解析正常，但结果错误。这个是由于druid的bug引起的。当VALUES[S]关键字错误时，druid会把这个错误的关键字当作别名处理。

> 其他须知

- [与MysqlServer的差异](https://actiontech.github.io/dble-docs-cn/6.Differernce_from_MySQL_Server/6.0_overview.html)
- [其他已知限制](https://actiontech.github.io/dble-docs-cn/5.Limit/5.2_other_limit.html)
- [语法兼容](https://actiontech.github.io/dble-docs-cn/3.SQL_Syntax/3.0_overview.html)
- [协议兼容](https://actiontech.github.io/dble-docs-cn/4.Protocol/4.0_overview.html)

> 架构图

![](images/architecture.png)

> Installation4Docker
```yaml script
# 通过docker compose执行一下命令,快速构建
version: '2'
networks:
    net:
        driver: bridge
        ipam:
            config:
                - subnet: 172.18.0.0/16
                  gateway: 172.18.0.253
services:
    mysql1:
        image: mysql:5.7
        container_name: backend-mysql1
        hostname: backend-mysql1
        privileged: true
        stdin_open: true
        tty: true
        ports:
            - "33061:3306"
        volumes:
            - ./:/etc/mysql/
        networks:
            net:
              ipv4_address: 172.18.0.2
        environment:
            MYSQL_ROOT_PASSWORD: 123456
    mysql2:
        image: mysql:5.7
        container_name: backend-mysql2
        hostname: backend-mysql2
        privileged: true
        stdin_open: true
        tty: true
        ports:
            - "33062:3306"
        volumes:
            - ./:/etc/mysql/
        networks:
            net:
              ipv4_address: 172.18.0.3
        environment:
            MYSQL_ROOT_PASSWORD: 123456
    dble-server:
        image: actiontech/dble:latest
        container_name: dble-server
        hostname: dble-server
        privileged: true
        stdin_open: true
        tty: true
        command: ["/opt/dble/bin/wait-for-it.sh", "backend-mysql1:3306","--","/opt/dble/bin/docker_init_start.sh"]
        ports:
            - "8066:8066"
            - "9066:9066"
        depends_on:
            - "mysql1"
            - "mysql2"
        networks:
            net:
              ipv4_address: 172.18.0.5
```

> 普通方式安装 链接:https://pan.baidu.com/s/1iGihrwS3RbogTFrtcsG9ng  密码:9sqs

- 下载之后解压就会得到一个dble的文件夹,进入该文件夹的conf文件夹来配置一下dble,一下只对主要文档做解释。详情请移步[官方文档](https://actiontech.github.io/dble-docs-cn/)
`rule.xml` 用来设置数据库的分片详情
`schema.xml` 用来设置数据主机配置,数据节点配置,数据库配置
`server.xml` 用来设置用户信息以及内存管理等设置，具体请查看[官方文档](https://actiontech.github.io/dble-docs-cn/)

- 配置完成之后就可以启动dble日志可以查看logs文件夹下的wrapper.log和dble.log
`wrapper.log` 是dble的启动日志
`dble.log` 是dble的运行日志
