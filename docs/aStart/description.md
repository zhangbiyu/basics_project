# 前言

<p align="center">
  <img src="./images/Business solutions .png"  class="no-zoom" />
</p>
<p align="center">
	<strong>An ordinary but great knowledge base</strong>
</p>
<p align="center">
	<a target="_blank" href="https://www.apache.org/licenses/LICENSE-2.0.html">
        <img src="https://img.shields.io/:license-Apache%20License%202.0-blue.svg" />
	</a>
	<a target="_blank" href="https://www.oracle.com/technetwork/java/javase/downloads/index.html">
		<img src="https://img.shields.io/badge/JDK-8+-green.svg" />
	</a>
</p>

#### 简介

基础项目构建模块，用于构建日常基础的项目模块。集成了一些常用的知识内容。

> basics_project名字的由来

中文译为:基础知识项目,这个项目的初衷是把一些经常用到的都总结起来,汇成一个资源池,为方便需要的时候可随时取用。
并用行动去向大家表述成为一个优秀的程序员都需要什么知识作为壁垒。

> basics_project如何改变我们学习的方式

我们在了解一个新知识点的时候,总是找各种博客,并且时间一长,积累的知识就多了。如果不太爱写文档的同学,可能学了A知识过一段时间就会忘掉。
所以我们是不是缺乏一个仓库,把这些东西都总结起来。现在只需要`basics_project`即可,里边会有很多经常用到的内容,并且汇总成一棵知识树
简单、快捷、高效的学习内容。让更多的时间花费在更多有意义的事情上。`多陪陪女朋友`,🤓不香吗？真是的,别总是让我为你们操心。

> 内容涉及

|技术名称| 版本号     |
|---| -------------- |
|springBoot|2.2.2.RELEASE|
|springCloud|Hoxton.SR1|
|myBatisPlus|3.2.0|
|maven|3.6.3|
|swagger|2.9.2|
|docker|1.13.1|

#### 常用工具包

- [Hutool](https://www.hutool.club/docs/#/) Java工具包
- [MyBatis-Plus](https://mybatis.plus/) 数据库Mybatis增强包
- [Guava](https://www.yiibai.com/guava) Google java核心库
- [FastJson](https://github.com/valyala/fastjson) 阿里JSON开源包
- [JSOUP](https://jsoup.org/) Html解析基于jquery版的解析工具
- [Swagger](https://swagger.io/) API文档工具
- [IJPay](https://gitee.com/javen205/IJPay) 微信支付宝支付包
- [lombok](https://projectlombok.org/) Java实用工具
